import math
import labs


while True:
    X_mass = []
    a, x_l, x_u,  x_method = 0, 0, 0, 0
    struct = [[], [], []]
    try:
        a = float(input('Enter a: '))
        x_l, x_u = float(input('Low ground x: ')), float(input('High ground x: '))
        fcn_num = int(input('Chose number[1-G/2-F/3-Y]: '))
        x_method = int(input('[1 - You enter steps| 2 - You enter step: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    x_steps, x_step, x = 0, 0, x_l
    x_old = x
    try:
        if x_method == 1:
            x_steps = int(input('Enter INTEGER steps value '))
            x_step = (x_u - x_l) / x_steps
        elif x_method == 2:
            x_step = float(input('Enter step value: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)
        # G
        while x <= x_u:
            try:
                G = (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
                print('---\nG: {:.3f} || X: {:.3f} |'.format(G, x))
                struct[0].append(G) #способ хранения и организация данных
                X_mass.append(x)
            except ZeroDivisionError:
                struct[0].append(None)
                X_mass.append(x)
            x += x_step
        # F
        X_mass = []
        x = x_old
        while x <= x_u:
            if not math.isclose(math.cos(72 * a**2 - 91 * a * x + 24 * x**2), 0, abs_tol=10 ** -1):
                F = math.cos(72 * a**2 - 91 * a * x + 24 * x**2)
                print('---\nF: {:.3f} || X: {:.3f} |'.format(F, x))
                X_mass.append(x)
                struct[1].append(F)
            else:
                X_mass.append(x)
                struct[1].append(None)
            x += x_step
        # Y
        X_mass = []
        x = x_old
        while x <= x_u:
            try:
                Y = math.asin(8 * a**2 - 14 * a * x + 5 * x**2)
                print('---\nY: {:.3f} || X: {:.3f} |'.format(Y, x))
                X_mass.append(x)
                struct[2].append(Y)
            except ValueError:
                print('---\nValueError\nY: NO || X: {:.3f} |'.format(x))
                X_mass.append(x)
                struct[2].append(None)
            x += x_step
        labs.plot_data(X_mass, struct[0], 'G(x)', 'Plot G(x)', 'x', 'G(x)')
        labs.maks_min_printer(struct[0], 'G')
        labs.plot_data(X_mass, struct[1], 'F(x)', 'Plot F(x)', 'x', 'F(x)')
        labs.maks_min_printer(struct[1], 'F')
        labs.plot_data(X_mass, struct[2], 'Y(x)', 'Plot Y(x)', 'x', 'Y(x)')
        labs.maks_min_printer(struct[2], 'Y')

    else:
        print('Error: function number not 1/2/3')

    while True:
        try:
            a = int(input("\nRepeat calculation? 1 - Yes | 0 - No "))
            break
        except ValueError:
            print('Input not correct. Repeat moore.')
        continue

    if a == 0:
        print('Have a nice day! :)')
        break
