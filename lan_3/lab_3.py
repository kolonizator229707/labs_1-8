import math
import labs


while True:
    X_mass, G_mass, F_mass, Y_mass = [], [], [], []
    a, x_l, x_u, fcn_num, x_method = 0, 0, 0, 0, 0

    try:
        a = float(input('Enter a: '))
        x_l, x_u = float(input('Low ground x: ')), float(input('High ground x: '))
        fcn_num = int(input('Chose number[1-G/2-F/3-Y]: '))
        x_method = int(input('[1 - You enter steps| 2 - You enter step: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    x_steps, x_step, x = 0, 0, x_l

    try:
        if x_method == 1:
            x_steps = int(input('Enter INTEGER steps value '))
            x_step = (x_u - x_l) / x_steps
        elif x_method == 2:
            x_step = float(input('Enter step value: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    if fcn_num == 1:
        while x <= x_u:
            try:
                G = (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
                print('---\nG: {:.3f} || X: {:.3f} |'.format(G, x))
                G_mass.append(G)
                X_mass.append(x)
            except ZeroDivisionError:
                G_mass.append(None)
                X_mass.append(x)
                print('---\n ERROR ZERO DIVISION\nG: NO || X: {:.3f} |'.format(x))
        x += x_step
        labs.plot_data(X_mass, G_mass, 'G(x)', 'Plot G(x)', 'x', 'G(x)')
    elif fcn_num == 2:
        while x <= x_u:
            if not math.isclose(math.cos(72 * a**2 - 91 * a * x + 24 * x**2), 0, abs_tol=10 ** -1):
                F = math.cos(72 * a**2 - 91 * a * x + 24 * x**2)
                print('---\nF: {:.3f} || X: {:.3f} |'.format(F, x))
                X_mass.append(x)
                F_mass.append(F)
            else:
                X_mass.append(x)
                F_mass.append(None)
                print('---\nF: NO || X: {:.3f} |'.format(x))
            x += x_step
        labs.plot_data(X_mass, F_mass, 'F(x)', 'Plot F(x)', 'x', 'F(x)')
    elif fcn_num == 3:
        while x <= x_u:
            try:
                Y = math.asin(8 * a**2 - 14 * a * x + 5 * x**2)
                print('---\nY: {:.3f} || X: {:.3f} |'.format(Y, x))
                X_mass.append(x)
                Y_mass.append(Y)
            except ValueError:
                print('---\nValueError\nY: NO || X: {:.3f} |'.format(x))
                X_mass.append(x)
                Y_mass.append(None)
            x += x_step
        labs.plot_data(X_mass, Y_mass, 'Y(x)', 'Plot Y(x)', 'x', 'Y(x)')
    else:
        print('Error: function number not 1/2/3')

    while True:
        try:
            a = int(input("\nRepeat calculation? 1 - Yes | 0 - No "))
            break
        except ValueError:
            print('Input not correct. Repeat moore.')
        continue

    if a == 0:
        print('Have a nice day! :)')
        break
