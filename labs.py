import matplotlib.pyplot as plt


def plot_data(x, y, lgnd='f(x)', title='Plot f(x)', xlabel='x', ylabel='f(x)'):  # Рисовалка графика
    plt.plot(x, y, 'ko-')
    plt.legend([lgnd])
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


def mass_cleaner(mass):  # Избавление массива от None
    result = []
    for i in mass:
        if i is None:
            continue
        else:
            result.append(i)
    if len(result) > 0:
        return result
    else:
        return 0


def mass_printer(mass):
    for element in mass:
        print('{:.3f}'.format(element), end=' ')


def mass_to_string(mass):
    string = ''
    for element in mass:
        string = string + str(element) + ' '
    return string


def search(mass):
    to_search = input('\nВведите шаблон для поиска совпадений, числа разделяйте 1-им пробелом\n')
    in_search = mass_to_string(mass)
    print('Колличество вхождений шаблона: ', in_search.count(to_search))


def maks_min_printer(mass, func_name):
    mass = mass_cleaner(mass)
    if mass != 0:
        print(f'\n-----\n{func_name} результаты:')
        print('\nМаксимальное значение функции {:}: {:.3f}'.format(func_name, max(mass)))
        print('Минимальное значение функции {:}: {:.3f}'.format(func_name, min(mass)))
        print('\n', mass_to_string(mass))
        search(mass)
    else:
        print(f'\n-----\n{func_name} решений нет')


def mass_to_file(g_mass, f_mass, y_mass, x_mass, directory):
    with open(directory, 'w') as f:
        print('G\tX', file=f)
        for i in range(len(g_mass)):
            try:
                print('{:.3f}\t{:.3f}'.format(g_mass[i], x_mass[i]), file=f)
            except TypeError:
                continue
        print('\nF\tX', file=f)
        for i in range(len(f_mass)):
            try:
                print('{:.3f}\t{:.3f}'.format(f_mass[i], x_mass[i]), file=f)
            except TypeError:
                continue
        print('\nY\tX', file=f)
        for i in range(len(y_mass)):
            try:
                print('{:.3f}\t{:.3f}'.format(y_mass[i], x_mass[i]), file=f)
            except TypeError:
                continue
