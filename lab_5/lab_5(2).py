number = input('Enter argument: ')
if number.count('.') != 0 or number.count(',') != 0:
    print('Argument not INT')
else:
    count = 0
    for i in number:
        if int(i) % 2 == 0:
            count += 1
    print('Number of even digits in the number: ', count)
