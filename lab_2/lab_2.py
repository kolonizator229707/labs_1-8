import math


x, a, fcn_num = 0, 0, 0
try:
    x, a, fcn_num = float(input("Введите 'x': ")), float(input("Введите 'a': ")), \
        int(input('Введите номер функции [1,2,3]^_^ '))
except ValueError:
    print('Введите число, а не символ ')
    exit(3)
if fcn_num == 1:
    try:
        G = (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
        print('G: {:.3f}'.format(G))
    except ZeroDivisionError:
        print('ERROR ZERO DIVISION\nG: NO ')
elif fcn_num == 2:
    F = math.cos(72 * a**2 - 91 * a * x + 24 * x**2)
    print('F: {:.3f}'.format(F))
elif fcn_num == 3:
    try:
        Y = math.asin(8 * a**2 - 14 * a * x + 5 * x**2)
        print('Y: {:.3f}'.format(Y))
    except ValueError:
        print('Не входит в область определения\nY: NO')
else:
    print('Номер операции неверный') 
