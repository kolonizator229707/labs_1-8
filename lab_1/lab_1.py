import math

x, a = float(input("Введите 'x': ")), float(input("Введите 'a': "))

G = (10 * (-45 * a**2 + 49 * a * x + 6 * x**2)) / (15 * a**2 + 49 * a * x + 24 * x**2)
F = math.cos(72 * a**2 - 91 * a * x + 24 * x**2)
Y = math.asin(8 * a**2 - 14 * a * x + 5 * x**2)

print('G: ', G, '\nF: ', F, '\nY: ', Y)
